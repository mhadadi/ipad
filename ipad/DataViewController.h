//
//  DataViewController.h
//  ipad
//
//  Created by Xcode Server on 29/05/1440 AH.
//  Copyright © 1440 Xcode Server. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *dataLabel;
@property (strong, nonatomic) id dataObject;

@end

