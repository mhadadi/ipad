//
//  RootViewController.h
//  ipad
//
//  Created by Xcode Server on 29/05/1440 AH.
//  Copyright © 1440 Xcode Server. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController <UIPageViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;

@end

