//
//  main.m
//  ipad
//
//  Created by Xcode Server on 29/05/1440 AH.
//  Copyright © 1440 Xcode Server. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
